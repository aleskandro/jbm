package org.unict.ing.jbm.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.unict.ing.jbm.domain.*;

public class ReservationStateTest {
	protected static Residence context;
	private static AccessKey savedKey;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		context = Residence.getInstance();
		final List<Room> rooms   = new ArrayList<Room>();
		final List<RoomDescription> roomsDescriptions = new ArrayList<RoomDescription>();
		final List<Service> services = new ArrayList<Service>();
		final List<ServiceDescription> servicesDescriptions = new ArrayList<ServiceDescription>();
		final List<Reservation> reservations  = new ArrayList<Reservation>();
		
		roomsDescriptions.add(new RoomDescription(1,0, new AgedPricingStrategy(25, 0), "Singola", "vista muro"));
		roomsDescriptions.add(new RoomDescription(2,0, new AgedPricingStrategy(75, 0), "Doppia", "vista muro"));
		roomsDescriptions.add(new RoomDescription(3,0, new AgedPricingStrategy(100, 0), "Tripla", "vista muro"));
		
		for (int i = 0; i < 6; i++) {
			rooms.add(new Room(roomsDescriptions.get(i % 3)));
		}
		
		servicesDescriptions.add(new ServiceDescription("Navetta", new UnaTantumPricingStrategy(50), "vista cane"));
		servicesDescriptions.add(new ServiceDescription("Bici", new UnaTantumPricingStrategy(10), "vista cane"));
		servicesDescriptions.add(new ServiceDescription("PensioneCompleta", new UnaTantumPricingStrategy(200), "vista cane"));
		
		int i = 0;
		for (ServiceDescription d: servicesDescriptions) {
			services.add(new Service(i++, d));
		}
		
		context.setReservations(reservations);
		context.setRooms(rooms);
		context.setServices(services);
		seed();
		seed();
	}
	
	public static void seed() {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		Boolean ret = controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 2, 0);
		List<List<Room>> solutions = controller.findSolutions();
		System.out.println("Solutions");
		int i = 0;
		for (List<Room> solution : solutions) {
			System.out.println("Solution n. " + i++);
			for (Room r : solution) {
				System.out.println(r.getDescription().getName() + "\t" + r.getDescription().getTotalAmount());
			}
		}
		
		System.out.println("Selecting Solution 0");
		controller.selectSolution(solutions.get(0));
		List<Service> availableServices = controller.getServices();
		List<Service> selectedServices = new ArrayList<Service>();
		selectedServices.add(availableServices.get(0));
		controller.associateServices(selectedServices);
		Reservation saved = controller.confirmReservation(new Customer("Mario Rossi", null, "me@mariorossi.it"));
		System.out.println(saved);
	}

	
	@Test
	public void testCheckin() {
		ReservationStateHandler controller = new ReservationStateHandler(context);
		boolean ret = controller.makeNewCheckin(0, "me@mariorossi.it");
		if (ret)
			fail("Reservation with the datas above does not exist but makeNewCheckin returned true");
		
		ret = controller.makeNewCheckin(1, "me@mariorossi.it");
		
		if (!ret)
			fail("The reservation should exists but makeNewCheckin returned false");
		
		while (controller.getGuestsToEnter() > 0) { 
			controller.enterGuest(new Guest("asdasd", new GregorianCalendar(1990, 2, 3), "Catania", "Via via", "AT123456", "RGJLKF12K93C304I"));
		}
		
		if (controller.getGuestsToEnter() > 0)
			fail("We are out of the loop, but the list of the guests was not completed");
		
		double caution = controller.getCautionNeeded();
		
		if (controller.getKey()  != null) {
			fail ("Caution is not still paid, but key was generated.");
		}
		
		ret = controller.enterPayment(caution);
		
		if (!ret)
			fail("Error on payments");
		
		if (ret) {
			savedKey = controller.getKey();
			System.out.println(savedKey);
		}
		
	}
	
	@Test
	public void testCheckOut() {
		ReservationStateHandler controller = new ReservationStateHandler(context);
		
		// This is a short checkin to set the key of a reservation and test the checkout
		boolean ret = controller.makeNewCheckin(2, "me@mariorossi.it");
		double caution = controller.getCautionNeeded();
		ret = controller.enterPayment(caution);
		savedKey = controller.getKey();
		
		ret = controller.enterKey(new AccessKey(0, "secret")); 
		
		if (ret) {
			fail ("Key not valid but enterKey returned true");
		}
		
		ret = controller.enterKey(savedKey);
		
		if (!ret) {
			fail("Error: key not valid? Return value was false");
		}
		
		double toPay = controller.getTotalToPay();
		
		ret = controller.enterPayment(toPay);
		
		if (!ret) {
			fail ("Error on enterPayment. return value was false.");
		}
		
		Invoice inv = controller.enterInvoiceDetails(new Invoice("Company", "CompanyAddess", "VAT"));
		System.out.println(inv);
		System.out.println(controller.getCurrentReservation());

	}
}
