/**
 * 
 */
package org.unict.ing.jbm.tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.unict.ing.jbm.domain.*;

/**
 * @author aleskandro
 *
 */
public class ProcessBookingTest {

	protected static Residence context;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		context = Residence.getInstance();
		final List<Room> rooms   = new ArrayList<Room>();
		final List<RoomDescription> roomsDescriptions = new ArrayList<RoomDescription>();
		final List<Service> services = new ArrayList<Service>();
		final List<ServiceDescription> servicesDescriptions = new ArrayList<ServiceDescription>();
		final List<Reservation> reservations  = new ArrayList<Reservation>();
		
		roomsDescriptions.add(new RoomDescription(1,0, new AgedPricingStrategy(25, 0), "Singola", "vista mare"));
		roomsDescriptions.add(new RoomDescription(2,0, new AgedPricingStrategy(75, 0), "Doppia", "vista mare"));
		roomsDescriptions.add(new RoomDescription(3,0, new AgedPricingStrategy(100, 0), "Tripla", "vista mare"));
		
		for (int i = 0; i < 6; i++) {
			rooms.add(new Room(roomsDescriptions.get(i % 3)));
		}
		
		servicesDescriptions.add(new ServiceDescription("Navetta", new UnaTantumPricingStrategy(50), "vista cane"));
		servicesDescriptions.add(new ServiceDescription("Bici", new UnaTantumPricingStrategy(10), "vista cane"));
		servicesDescriptions.add(new ServiceDescription("PensioneCompleta", new UnaTantumPricingStrategy(200), "vista cane"));
		
		int i = 0;
		for (ServiceDescription d: servicesDescriptions) {
			services.add(new Service(i++, d));
		}
		
		context.setReservations(reservations);
		context.setRooms(rooms);
		context.setServices(services);
		
	}

	/**
	 * Test method for the UseCase UC1
	 */
	@Test
	public void testReserve() {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 2, 0);
		
		List<List<Room>> solutions = controller.findSolutions();
		System.out.println("Solutions");
		int i = 0;
		for (List<Room> solution : solutions) {
			System.out.println("Solution n. " + i++);
			for (Room r : solution) {
				System.out.println(r.getDescription().getName() + "\t" + r.getDescription().getTotalAmount());
			}
		}
		
		System.out.println("Selecting Solution 0");
		controller.selectSolution(solutions.get(0));
		List<Service> availableServices = controller.getServices();
		List<Service> selectedServices = new ArrayList<Service>();
		selectedServices.add(availableServices.get(0));
		controller.associateServices(selectedServices);
		Reservation saved = controller.confirmReservation(new Customer("Mario Rossi", null, null));
		System.out.println(saved);
		assertEquals(saved,context.getReservations().get(context.getReservations().size() - 1));
	}
	
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void testMakeNewReservationDateRange() throws RuntimeException {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		thrown.expectMessage("DateRange");
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 1)), 
			1, 0);
	}
	
	@Test
	public void testMakeNewReservationPeople() throws RuntimeException {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		thrown.expectMessage("adults");
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 
			0, 0);
	}
	
	@Test
	public void testFindSolutionsPeopleLimits() {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 
				10, 0);
		List<List<Room>> solutions = controller.findSolutions();
		assertEquals(solutions.size(), 1);
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 
				11, 0);
		solutions = controller.findSolutions();
		assertEquals(solutions.size(), 0);
		
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 3)), 
				1, 0);
		solutions = controller.findSolutions();
		assertEquals(solutions.size(), 6);
	}
	
	@Test(expected=IndexOutOfBoundsException.class)
	public void testFindSolutionsWithOverlapping() {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		Random random = new Random();

		int maxSolutions = -1;
		do {
			controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1 + random.nextInt(2)), new GregorianCalendar(2017, 9, 10 - random.nextInt(2))),
					2, 0);
			List<List<Room>> solutions = controller.findSolutions();
			controller.selectSolution(solutions.get(0));
			controller.confirmReservation(new Customer("Mario Rossi", null, null));
			// This is an hack to avoid infinite loop and keep the test and setup-independent as possible
			if (maxSolutions == -1) 
				maxSolutions = solutions.size();
			else
				maxSolutions--;
		} while (maxSolutions > 0);
	}
}
