/**
 * 
 */
package org.unict.ing.jbm.tests;

import static org.junit.Assert.*;



import org.junit.BeforeClass;
import org.junit.Test;
import org.unict.ing.jbm.domain.*;

/**
 * @author aleskandro
 *
 */
public class ResidenceTest {

	/**
	 * @throws java.lang.Exception
	 */
	private static Residence context = Residence.getInstance();
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}
	
	@Test
	public void testAddRooms() {
		RoomDescription d = new RoomDescription(1, 0, new AgedPricingStrategy(50,0), "Singola", "Vista sul mare\nWifi incluso");
		if (!context.addRoomsDescription(d))
			fail("error adding Room Description");
		boolean ret = context.addRoom(d, 10);
		if (!ret)
			fail("Error adding rooms");
		
		d = new RoomDescription(2, 0, new AgedPricingStrategy(60,0), "Doppia", "Vista sul mare\nWifi incluso");
		if (!context.addRoomsDescription(d))
			fail("error adding Room Description");
		
		ret = context.addRoom(d, 10);
		if (!ret)
			fail("Error adding rooms");
		
		d = new RoomDescription(0, 0, null, "Doppia", null);
		if (context.addRoomsDescription(d))
			fail("error adding Room Description: it was considered correct, but this latter one was not");
		
		d = new RoomDescription(2, 0, null, "Doppia", null);
		if (context.addRoomsDescription(d))
			fail("error adding Room Description: it was considered correct, but this latter one was not");
		
		System.out.println(context.getRooms());
	}
	
	@Test
	public void testAddService() {
		ServiceDescription d = new ServiceDescription("Navetta",  new UnaTantumPricingStrategy(50), "Navetta dall'aeroporto");
		
		if (!context.addServicesDescription(d))
			fail("error adding Service Description");
		
		boolean ret = context.addService(d);
		if (!ret)
			fail("Error adding service");
		
		d = new ServiceDescription("Ristorante",  new UnaTantumPricingStrategy(50), "Pensione completa");
		
		if (!context.addServicesDescription(d))
			fail("error adding Service Description");
		ret = context.addService(d);
		if (!ret)
			fail("Error adding service");
		
		d = new ServiceDescription("Navetta",  null, "Navetta dall'aeroporto");
		
		if (context.addServicesDescription(d))
			fail("error adding Service Description: price is mandatory and was not provided but the ServiceDescription was added anyway");
		
		System.out.println(context.getServices());
	}

}
