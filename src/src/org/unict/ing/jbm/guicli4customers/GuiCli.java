package org.unict.ing.jbm.guicli4customers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.unict.ing.jbm.domain.Residence;
import org.unict.ing.jbm.guicommon.CommonVC;
import org.unict.ing.jbm.guicommon.GuiHelpers;

class GuiCli extends GuiHelpers {

	private static Residence context = Residence.getInstance();
	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	private static DateFormat edf = new SimpleDateFormat("MM/yyyy");

	private static void mainMenu() {
		while(true) {
			Integer reply = 0;
			
			String menu = "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "+++++++++++++ jBM - Booking Manager +++++++++++++ \n" +
						  "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "|                                               | \n" +				
					      "| 1) Crea prenotazione                          | \n" +
					      "| 2) Lista stanze                               | \n" +
						  "| 3) Lista servizi                              | \n" +
					      "| 4) Checkin                                    | \n" +
					      "| 5) Checkout                                   | \n" +
					      "| 0) Esci                                       | \n" +
					      "+++++++++++++++++++++++++++++++++++++++++++++++++ \n\n" +
						  "--> ";
						  		
			reply = readInt(menu);		  
			switch(reply) {
			case 1:
				CommonVC.makeReservation();
				break;
			case 2:
				CommonVC.listRooms();
				break;
			case 3:
				CommonVC.listServices();
				break;
			case 4:
				CommonVC.makeCheckin();
				break;
			case 5:
				CommonVC.makeCheckOut();
				break;
			case 0:
				return;
			default:
				continue;
			}
			
			pause();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommonVC.setup(context, df, edf);
		CommonVC.seeds();
		mainMenu();
		print("Exiting");
		sc.close();
	}

}
