package org.unict.ing.jbm.guicommon;

import java.util.Scanner;

public abstract class GuiHelpers {
	protected static Scanner sc = new Scanner(System.in);
	
	/* Alias for verbose method System.out.print */
	protected static void print(Object... args)  {
		for (Object arg : args)
			System.out.print(arg);
	}
	
	/* Alias for verbose method System.out.println */
	protected static void println(Object... args)  {
		for (Object arg : args)
			System.out.println(arg);
	}

	protected static void pause() {
		println("Premi qualunque tasto per tornare al menu principale...");
		sc.nextLine();
	}
	
	protected static Integer readInt(String message) {
		print(message);
		Integer ret = sc.nextInt();
		sc.nextLine();
		return ret;
	}
	
	protected static String readLine(String message) {
		print(message);
		String ret = sc.nextLine();
		return ret;
	}
	
	protected static Double readDouble(String message) {	
		print(message);
		Double ret = sc.nextDouble();
		sc.nextLine();
		return ret;
	}

}
