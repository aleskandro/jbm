package org.unict.ing.jbm.guicommon;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.unict.ing.jbm.domain.AccessKey;
import org.unict.ing.jbm.domain.AgedPricingStrategy;
import org.unict.ing.jbm.domain.Customer;
import org.unict.ing.jbm.domain.DateRange;
import org.unict.ing.jbm.domain.Guest;
import org.unict.ing.jbm.domain.Invoice;
import org.unict.ing.jbm.domain.ProcessBookingHandler;
import org.unict.ing.jbm.domain.Reservation;
import org.unict.ing.jbm.domain.ReservationStateHandler;
import org.unict.ing.jbm.domain.Residence;
import org.unict.ing.jbm.domain.Room;
import org.unict.ing.jbm.domain.RoomDescription;
import org.unict.ing.jbm.domain.Service;
import org.unict.ing.jbm.domain.ServiceDescription;
import org.unict.ing.jbm.domain.UnaTantumPricingStrategy;

public class CommonVC extends GuiHelpers {
	private static Residence context = Residence.getInstance();
	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	private static DateFormat edf = new SimpleDateFormat("MM/yyyy");
	
	public static void setup(Residence c, DateFormat dff, DateFormat edff) {
		context = c;
		df = dff;
		edf = edff;
	}
	
	public static boolean paymentMenu(double amount, ReservationStateHandler controller) {
		while(true) {
			Integer reply = 0;
			
			String menu = "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "+++++++++++++  Metodo di pagamento  +++++++++++++ \n" +
						  "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "|                                               | \n" +				
					      "| 1) Contanti                                   | \n" +
						  "| 2) Carta di credito                           | \n" +
					      "+++++++++++++++++++++++++++++++++++++++++++++++++ \n\n" +
						  "--> ";
						  		
			reply = readInt(menu);		  
			switch(reply) {
			case 1:
				return makeCashPayment(amount, controller);
			case 2:
				return makeCreditPayment(amount, controller);
			/*case 0:
				return;*/
			default:
				continue;
			}	
		}
	}
	
	public static boolean makeCreditPayment(double amount, ReservationStateHandler controller) {
		println("Pagamento di eur " + amount + " con carta di credito");
		String cNum = readLine("Inserire numero della carta di credito: ");
		String type = readLine("Inserire tipo: ");
		GregorianCalendar expiry = new GregorianCalendar();
		while (true) {
			try {
				expiry.setTime(edf.parse(readLine("Inserire la data di scadenza (MM/YYYY): ")));
				break;
			} catch (ParseException e) {
				println("Errore nell'inserimento della data.");
			}
		}
		int cvv = readInt("Inserire cvv: ");
		
		println("Pagamento in corso...");
		
		if (controller.enterPayment(amount, cNum, expiry, cvv, type)) {
			println("Pagamento avvenuto con successo...");
			return true;
		}
		
		return false;
	}
	
	public static boolean makeCashPayment(double amount, ReservationStateHandler controller) {
		println("Pagamento di eur " + amount + " in contanti in corso...");
		return controller.enterPayment(amount);
	}
	
	public static void enterGuests(ReservationStateHandler controller) {
		int i = 0;
		while(controller.getGuestsToEnter() > 0) {
			boolean ret = false;
			while (!ret) {
				println("Ospite n. " + (++i));
				GregorianCalendar bornDate = new GregorianCalendar();
				String name       = readLine("Inserire nome ospite: ");
				
				while (true) {
					try {
						bornDate.setTime(df.parse(readLine("Inserire la data di nascita (dd/MM/YYYY): ")));
						break;
					} catch (ParseException e) {
						println("Errore nell'inserimento della data.");
					}
				}
				
				String bornCity   = readLine("Inserire citta' di nascita: ");
				String address    = readLine("Inserire indirizzo completo di residenza: ");
				String nationalID = readLine("Inserire n. documento di identita': ");
				String taxCode    = readLine("Inserire codice fiscale: ");
				
				
				ret = controller.enterGuest(new Guest(
							name, bornDate, bornCity, address, nationalID, taxCode));
				if (!ret) {
					println("Errore nell'inserimento dei dati dell'ospite.");
				}
			}
		}
	}
	
	public static void makeCheckin() {
		ReservationStateHandler controller = new ReservationStateHandler(context);
		int reservationId      = readInt("Inserire id prenotazione: ");
		String reservationMail = readLine("Inserire E-Mail prenotazione: ");
		if (!controller.makeNewCheckin(reservationId, reservationMail)) {
			println("Non e' stata trovata la prenotazione cercata o il check-in e' gia' stato effettuato.");
			return;
		}
			
		println(controller.getCurrentReservation().toString());
		
		println("Inserimento dei dati per gli ospiti...");
		
		enterGuests(controller);
		
		double caution = controller.getCautionNeeded();
		println("La cauzione da pagare ammonta a: " + caution);
		
		if (!paymentMenu(caution, controller)) {
			println("Errore nel pagamento...");
			return;
		};

		AccessKey key = controller.getKey();
		println("La tua chiave di Accesso: " + key);
		println("Check-in completato.");
		pause();
	}
	
	
	public static void makeCheckOut() {
		ReservationStateHandler controller = new ReservationStateHandler(context);
		int    accessKeyId     = readInt("Inserire id chiave di accesso: ");
		String accessKeySecret = readLine("Inserisci codice chiave di accesso: ");
		
		if (!controller.enterKey(new AccessKey(accessKeyId,accessKeySecret))) {
			println("Nessuna prenotazione trovata o chiave di accesso non valida o impossibile procedere attualmente al check-out.");
			return;
		}
			
		double total = controller.getTotalAmount();
		println("Il totale ammonta a (eur): " + total);
		double toPay = controller.getTotalToPay();
		println("Totale da pagare: " + toPay);
		
		if (!paymentMenu(toPay, controller)) {
			println("Pagamento non andato a buon fine");
			return;
		}
		println("Inserimento dati di fatturazione...");
		String companyName = readLine("Inserire nome (o azienda): ");
		String companyAddress = readLine("Inserire indirizzo: ");
		String vat = readLine("Inserisci P.Iva o codice fiscale: ");
		
		Invoice inv = controller.enterInvoiceDetails(new Invoice(companyName, companyAddress, vat));
		println(inv);	
		println(controller.getCurrentReservation().toString());
		
		println("Grazie e arrivederci.");
		pause();
	}
	
	public static void listRooms() {
		println(context.getRooms());
	}
	
	public static void listServices() {
		println(context.getServices());
	}

	public static void makeReservation() {
		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		
		GregorianCalendar dateIn    = new GregorianCalendar();
		GregorianCalendar dateOut   = new GregorianCalendar();
		DateRange         dateRange = null;
		try {
			dateIn.setTime(df.parse(readLine("Inserire la data di Check-in (dd/MM/YYYY): ")));
			dateOut.setTime(df.parse(readLine("Inserire la data di Check-out (dd/MM/YYYY): ")));
			dateRange = new DateRange(dateIn, dateOut);
		} catch (ParseException e) {
			println("Errore nell'inserimento della data.");
			makeReservation();
		}
		
		Integer adults   = readInt("Inserire numero di adulti: ");
		Integer children = readInt("Inserire numero di bambini: ");
		
		controller.makeNewReservation(dateRange, adults, children);
		
		List<Service> availableServices = controller.getServices();
		List<Service> selectedServices = new ArrayList<Service>();
		
		List<List<Room>> solutions = controller.findSolutions();
		if (solutions.size() == 0) {
			println ("Nessuna soluzione per le date selezionate");
			return;
		}
			
		println("Seleziona la soluzione desiderata: ");
		int i = 0;
		for (List<Room> solution : solutions) {
			Double total = 0.0;
			print(i++ + ")");
			for (Room r : solution) {
				println(r.getDescription());
				total += r.getDescription().getTotalAmount();
			}
			println("Totale: " + total);
			println();
		}
		
		println();
		
		Integer choice = readInt("[-1: annulla] --> ");
		if (choice == -1)
			return;
		controller.selectSolution(solutions.get(choice));
		
		println("Selezionare i servizi:");
		i = 0;
		for (Service s : availableServices) {
			print(i++ + ")");
			print(s.getDescription());
		}
		print();
		
		choice = -1;
		
		while (choice != 0) {
			choice = readInt("([id Servizio o 0 per terminare la selezione] --> ");
			if (choice <= 0 || choice >= availableServices.size())
				continue;
			selectedServices.add(availableServices.get(choice));
		}
		
		controller.associateServices(selectedServices);
		
		String customerName  = "";
		String customerPhone = "";
		String customerMail   = "";
		do {
			customerName  = readLine("Inserire il nome (obbligatorio): ");
			customerPhone = readLine("Inserire telefono: ");
			customerMail  = readLine("Inserire e-mail (obbligatorio): ");
		} while (customerMail.length() <= 0 || customerName.length() <= 0);
		
		
		Reservation saved = controller.confirmReservation(new Customer(customerName, customerPhone, customerMail));
		
		String pay = readLine("Vuoi pagare adesso con carta di credito? (s per confermare)");
		
		if (pay.equals("s")) {
			ReservationStateHandler c2 = new ReservationStateHandler(context);
			c2.makeNewCheckin(saved.getId(), saved.getCustomer().getMailAddress());
			makeCreditPayment(c2.getCautionNeeded(), c2);
		}
		println(saved);
	}
	
	public static void seeds() { 
		RoomDescription d = new RoomDescription(1, 0, new AgedPricingStrategy(50,0), "Singola", "Vista sul mare\nWifi incluso");
		context.addRoomsDescription(d);
		context.addRoom(d, 1);
		
		d = new RoomDescription(2, 0, new AgedPricingStrategy(60,0), "Doppia", "Vista sul mare\nWifi incluso");
		context.addRoom(d, 1);
		
		ServiceDescription ds = new ServiceDescription("Navetta",  new UnaTantumPricingStrategy(50), "Navetta dall'aeroporto");
		
		context.addServicesDescription(ds);

		ProcessBookingHandler controller = new ProcessBookingHandler(context);
		
		controller.makeNewReservation(new DateRange(new GregorianCalendar(2017, 9, 1), new GregorianCalendar(2017, 9, 5)), 2, 0);
		List<List<Room>> solutions = controller.findSolutions();
		controller.selectSolution(solutions.get(0));
		List<Service> selectedServices = new ArrayList<Service>();
		controller.associateServices(selectedServices);
		controller.confirmReservation(new Customer("Mario Rossi", "Test", "Test"));
	
	}
}
