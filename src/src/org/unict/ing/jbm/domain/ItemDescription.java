/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
interface ItemDescription {
	public String getName();
	public void   setName(String name);
	
	public IPrice getPrice();
	public void   setPrice(IPrice p);
	
	public String getExtraInfos();
	public void setExtraInfos(String extrainfos);
	
	public double getTotalAmount();
	public Integer getAdultsCapacity();
	public Integer getChildrenCapacity();
	String toString(Reservation r);
	double getTotalAmount(Reservation reservation);
}
