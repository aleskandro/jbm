package org.unict.ing.jbm.domain;

final class ReservationItem {
	private Reservation reservation;
	private Item 		item;
	
	private IPrice      price;

	ReservationItem(Reservation r, Item i, IPrice p) {
		this.reservation = r;
		this.item        = i;
		this.price       = p;
	}
	Reservation getReservation() {
		return reservation;
	}

	Item getItem() {
		return item;
	}

	IPrice getPrice() {
		return price;
	}

	void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}

	void setItem(Item item) {
		this.item = item;
	}

	void setPrice(IPrice price) {
		this.price = price;
	}
	
	
}
