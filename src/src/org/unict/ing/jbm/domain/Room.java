/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
public final class Room implements Item {
	
	private static int lastid = 0;
	private Integer id;
	private RoomDescription description;
	
	public Room(RoomDescription description)
	{
		setId(++lastid);
		this.description = description;
	}
	
	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public RoomDescription getDescription() {
		return this.description;
	}

	@Override
	public void setDescription(ItemDescription d) {
		this.description = (RoomDescription) d;
	}
    public String toString() {
    	
        String out =  "\nRoom n." + this.id + "\n" +
        		"Name of the Room:" + this.getDescription().getName() + "\n" +
                "Adult Capacity:" + this.getDescription().getAdultsCapacity() + "\n" +
                "Children Capacity:" + this.getDescription().getChildrenCapacity() + "\n" +
                "Total Amount:" + this.getDescription().getTotalAmount() + "\n" +
                "People Capacity:" + this.getDescription().getPeopleCapacity() + "\n";
        return out;
    }

}
