/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
interface Item {
	Integer getId();
	void    setId(Integer id);
	
	ItemDescription getDescription();
	void            setDescription(ItemDescription d);
	
}
