 package org.unict.ing.jbm.domain;

import java.util.GregorianCalendar;



public final class ReservationStateHandler {
	
	private Reservation currentReservation;
	private Residence context;
	
	public ReservationStateHandler(Residence context)
	{
		this.context = context;	
	}
	
	public Reservation getCurrentReservation() {
		return currentReservation;
	}
	
	public boolean makeNewCheckin(int reservationId, String customerEmail) {
		this.currentReservation = this.context.findReservationByIdCustomerMail(reservationId,customerEmail);
		return this.currentReservation != null;
	}
	
	public boolean enterGuest(Guest guest) {
		return this.currentReservation.addGuest(guest);
	}
	
	public int getGuestsToEnter() {
		return this.currentReservation.getPeopleCount() - this.currentReservation.getGuests().size();
	}
	public double getCautionNeeded() {
		return this.currentReservation.checkForCaution();
	}
	
	public AccessKey getKey() {
		return this.currentReservation.generateKey();
	}

	public boolean enterKey(AccessKey key) {
		return ((this.currentReservation = this.context.findReservationByAccessKey(key)) != null);
	}
	
	public double getTotalAmount() {
		return currentReservation.getTotalAmount();
	}
	
	
	public boolean enterPayment(double amount) {
		return currentReservation.addPayment(amount);		
	}
	
	public boolean enterPayment(double amount, String cNum, GregorianCalendar expiry, int cvv, String type) {
		return currentReservation.addPayment(cNum,  expiry, cvv, type, amount);
	}
	
	public Invoice getInvoice() {
		return currentReservation.getInvoice();
	}
	
	public Invoice enterInvoiceDetails(Invoice invoice) {
		return currentReservation.generateInvoice(invoice);
	}
	
	public double getTotalToPay() {
		return currentReservation.getTotalToPay();	
	}	
}
