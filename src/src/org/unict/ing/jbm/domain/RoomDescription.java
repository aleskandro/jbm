/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
public final class RoomDescription implements ItemDescription {
	private Integer adultsCapacity;
	private Integer childrenCapacity;
	private AgedPricingStrategy price;
	private String name;
	private String extraInfos;
	
	public RoomDescription(Integer adultsCapacity, Integer childrenCapacity, AgedPricingStrategy price, String name, String extraInfos)
	{
		this.adultsCapacity   = adultsCapacity;
		this.childrenCapacity = childrenCapacity;
		this.setPrice(price);
		this.name             = name;
		this.extraInfos		  = extraInfos;
	}
	
	Integer getPeopleCapacity() {
		return this.getAdultsCapacity() + this.getChildrenCapacity();
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public AgedPricingStrategy getPrice() {
		return this.price;
	}

	@Override
	public void setPrice(IPrice p) {
		this.price = (AgedPricingStrategy) p;
	}
	
	@Override
	public double getTotalAmount() {
		return this.getPrice().getTotal(this, null);
	}
	
	@Override
	public double getTotalAmount(Reservation r) {
		return this.getPrice().getTotal(this, null);
	}
	
	@Override
	public Integer getAdultsCapacity() {
		return adultsCapacity;
	}
	
	void setAdultsCapacity(Integer adultsCapacity) {
		this.adultsCapacity = adultsCapacity;
	}
	
	public Integer getChildrenCapacity() {
		return childrenCapacity;
	}
	
	void setChildrenCapacity(Integer childrenCapacity) {
		this.childrenCapacity = childrenCapacity;
	}
	
	void setPrice(AgedPricingStrategy price) {
		this.price = price;
	}

	@Override
	public String getExtraInfos() {
		return this.extraInfos;
	}

	@Override
	public void setExtraInfos(String extraInfos) {
		this.extraInfos = extraInfos;
	}
	

	@Override
	public String toString() {
		return "\n" + getName() + "\t" + getTotalAmount();	
	}
	
	@Override
	public String toString(Reservation r) {
		return "\n" + getName() + "\t" + getTotalAmount(r);	
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) 
	        return false;
	    try {
	    	RoomDescription d = (RoomDescription)obj;
	    	if ( this.getAdultsCapacity().equals(d.getAdultsCapacity())
	    			&& this.getChildrenCapacity().equals(d.getChildrenCapacity())
	    			&& this.getName().equals(d.getName()))
	    		return true;
	    } catch (Exception e) {
	    	return false;
	    }
	    return false;
	}

}
