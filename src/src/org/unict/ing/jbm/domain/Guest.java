package org.unict.ing.jbm.domain;

import java.util.GregorianCalendar;

public class Guest {

	private String fullName;
	private GregorianCalendar bornDate;
	private String bornCity;
	private String fullAddress;
	private String nationalId;
	private String taxCode;
	
	public Guest(String fullName, GregorianCalendar bornDate, String bornCity, String fullAddress, String nationalId, String taxCode) {
		this.fullName = fullName;
		this.bornDate = bornDate;
		this.fullAddress = fullAddress;
		this.nationalId = nationalId;
		this.taxCode = taxCode;		
		this.bornCity = bornCity;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public GregorianCalendar getBornDate() {
		return bornDate;
	}

	public void setBornDate(GregorianCalendar bornDate) {
		this.bornDate = bornDate;
	}

	public String getBornCity() {
		return bornCity;
	}

	public void setBornCity(String bornCity) {
		this.bornCity = bornCity;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getNationalId() {
		return nationalId;
	}

	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}
	
	public String toString() {
		String out = "";
		out += "Name: " + this.getFullName() + "\n";
		out += "Address: " + this.getFullAddress() + "\n";
		out += "BornCity: " + this.getBornCity() + "\n";
		out += "BornDate: " + this.getBornCity() + "\n";
		out += "NationalID: " + this.getNationalId() + "\n";
		out += "TaxCode: " + this.getTaxCode() + "\n";
		
		return out;
	}
	
}
