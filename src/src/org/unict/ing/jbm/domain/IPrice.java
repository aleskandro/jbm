/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
public interface IPrice 
{
	public double getTotal(ItemDescription description, Reservation reservation);
}
