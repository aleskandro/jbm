/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
public final class Service implements Item {

	private static int lastid = 0;
	private Integer id;
	private ServiceDescription description;

	public Service(Integer id, ServiceDescription d)
	{
		setId(++lastid);
		this.description = d;
	}
	@Override
	public Integer getId() {
		return this.id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public ServiceDescription getDescription() {
		return (ServiceDescription) this.description;
	}

	@Override
	public void setDescription(ItemDescription d) {
		this.description = (ServiceDescription) d;
	}
    public String toString() {
    	
        String out =  "\nService n." + this.id + "\n" + this.getDescription();
        return out;
    }

}
