/**
 * 
 */
package org.unict.ing.jbm.domain;

/**
 * @author aleskandro
 *
 */
public final class ServiceDescription implements ItemDescription {

	private String name;
	private UnaTantumPricingStrategy price;
	private String extraInfos;
	
	public ServiceDescription(String name, UnaTantumPricingStrategy price, String extraInfos) {
		this.name  = name;
		this.price = price;
		this.extraInfos = extraInfos;
	}
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public UnaTantumPricingStrategy getPrice() {
		return this.price;
	}

	@Override
	public void setPrice(IPrice p) {
		this.price = (UnaTantumPricingStrategy) p;
	}
	
	@Override
	public double getTotalAmount() {
		return this.getPrice().getTotal(this, null);
	}
	
	@Override
	public double getTotalAmount(Reservation reservation) {
		return this.getPrice().getTotal(this, reservation);
	}
	
	@Override
	public String getExtraInfos() {
		return this.extraInfos;
	}
	@Override
	public void setExtraInfos(String extraInfos) {
		this.extraInfos = extraInfos;
	}
	
	@Override
	public String toString() {
		return  "\nName of the Service:" + this.getName() + "\n" +
                "Extra info:" + this.getExtraInfos() + "\n"+
        		"Total Amount:" + this.getTotalAmount() + "\n";
	}
	
	// Used when need to get price based on Reservation fields as people
	@Override
	public String toString(Reservation reservation) {
		return  "\nName of the Service:" + this.getName() + "\n" +
                "Extra info:" + this.getExtraInfos() + "\n"+
        		"Total Amount:" + this.getTotalAmount(reservation) + "\n";
	}
	
	// These methods are not needed for Service class; they are implemented as neutrals elements
	@Override
	public Integer getAdultsCapacity() {
		return 1;
	}
	@Override
	public Integer getChildrenCapacity() {
		return 1;
	}
}
