package org.unict.ing.jbm.domain;

import java.util.GregorianCalendar;

class CreditPayment extends IPayment {

	CreditCard creditCard;

	CreditCard createCc(String cNum, GregorianCalendar expiry, int cvv, String type){
		 CreditCard creditCard = new CreditCard(cNum,expiry,cvv,type);
		 return creditCard;
	}
		
	CreditPayment(String cNum, GregorianCalendar expiry, int cvv, String type) {
		this.creditCard = this.createCc(cNum,expiry,cvv,type);
	}

	@Override
	public boolean authorize(double amount) {
		CreditAuthorizationService cService = new CreditAuthorizationService();
		this.setAmount(amount);
		
		return cService.authorize(this.creditCard, amount);
	}
	
}