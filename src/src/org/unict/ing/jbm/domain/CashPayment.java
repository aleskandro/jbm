package org.unict.ing.jbm.domain;

final class CashPayment extends IPayment {

	@Override
	public boolean authorize(double amount) {
		this.setAmount(amount);
		return true;
	}

}
