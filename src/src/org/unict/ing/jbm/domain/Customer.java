package org.unict.ing.jbm.domain;

public final class Customer {
	private String fullName;
	private String phoneNumber;
	private String mailAddress;
	
	public Customer(String fullName, String phoneNumber, String mailAddress) {
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.mailAddress = mailAddress;
	}
	
	public String getFullName() {
		return fullName;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public String getMailAddress() {
		return mailAddress;
	}
	
	
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	
	public String toString() {
		return this.fullName + "\t" + this.phoneNumber + "\t" + this.mailAddress;
	}
}
