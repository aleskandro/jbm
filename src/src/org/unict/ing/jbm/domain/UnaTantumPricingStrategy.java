package org.unict.ing.jbm.domain;

public final class UnaTantumPricingStrategy implements IPrice {
	private double price;
	
	public UnaTantumPricingStrategy(UnaTantumPricingStrategy duplicand) {
		this.setPrice(duplicand.getPrice());
	};
	
	public UnaTantumPricingStrategy(double p) {
		this.setPrice(p);
	}

	double getPrice() {
		return price;
	}

	void setPrice(double price) {
		this.price = price;
	}

	@Override
	public double getTotal(ItemDescription description, Reservation reservation) {
		return this.price;
	}
	
}
