package org.unict.ing.jbm.domain;

class CreditAuthorizationService {
	
	boolean authorize(CreditCard creditCard, Double amount) {
		String req = makeRequest();
		return processReply(sendRequest(req));
	}
	
	String makeRequest() {
		return "";
	}
	
	boolean processReply(String reply) {
		return true;
	}
	
	String sendRequest(String request) {
		return "";
	}
}