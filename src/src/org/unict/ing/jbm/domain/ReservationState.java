package org.unict.ing.jbm.domain;

abstract class ReservationState {

	public String toString() {
		return this.getClass().getSimpleName();
	}
}
