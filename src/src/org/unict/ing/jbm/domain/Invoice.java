package org.unict.ing.jbm.domain;

public class Invoice {

	private static int lastid = 0;
	private int id;
	private String fullName;
	private String fullAddress;
	private String vat;
	
	public Invoice(String fullName, String fullAddress, String vat) {
		
		setId(++lastid);
		this.fullName = fullName;
		this.fullAddress = fullAddress;
		this.vat = vat;
		
	}

	public int getId() {
		return id;
	}

	void setId(int id) {
		this.id = id;
	}

	public String getFullName() {
		return fullName;
	}

	void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getVat() {
		return vat;
	}

	void setVat(String vat) {
		this.vat = vat;
	}
}
