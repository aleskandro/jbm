package org.unict.ing.jbm.domain;

public final class AgedPricingStrategy implements IPrice {
	private double priceAdult;
	private double priceChild;
	
	public AgedPricingStrategy(AgedPricingStrategy duplicand) {
		this.setPriceAdult(duplicand.getPriceAdult());
		this.setPriceChild(duplicand.getPriceChild());
	};
	
	public AgedPricingStrategy(double pAdult, double pChild) {
		this.setPriceAdult(pAdult);
		this.setPriceChild(pChild);
	}
	
	double getPriceAdult() {
		return priceAdult;
	}

	private void setPriceAdult(double priceAdult) {
		this.priceAdult = priceAdult;
	}

	double getPriceChild() {
		return priceChild;
	}

	private void setPriceChild(double priceChild) {
		this.priceChild = priceChild;
	}

	@Override
	public double getTotal(ItemDescription description, Reservation reservation) {
		double totalAmount = 0;
		int adults   = description.getAdultsCapacity();
		int children = description.getChildrenCapacity();
		
		totalAmount += adults   * this.getPriceAdult();
		totalAmount += children * this.getPriceChild();
		return totalAmount;
	}

}
