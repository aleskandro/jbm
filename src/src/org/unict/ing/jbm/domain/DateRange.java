package org.unict.ing.jbm.domain;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class DateRange {
	private GregorianCalendar dateIn;
	private GregorianCalendar dateOut;
	static SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
	public DateRange(GregorianCalendar checkin, GregorianCalendar checkout) {
		this.dateIn = checkin;
		this.dateOut = checkout;
		if (this.getDays() <= 0) 
			throw new RuntimeException("Invalid DateRange provided");
	}
	public GregorianCalendar getDateIn() {
		return dateIn;
	}
	
	public GregorianCalendar getDateOut() {
		return dateOut;
	}

	public boolean overlap(DateRange that) {
		// Check if two date ranges have a not null intersection
		if (this.getDateIn().before(that.getDateOut()) && this.getDateOut().after(that.getDateIn()))
			return true;
		return false;
	}
	
	public String toString() {
		String out = "";
		fmt.setCalendar(this.getDateIn());
		out += fmt.format(this.getDateIn().getTime());
		out += " <---> ";
		fmt.setCalendar(this.getDateOut());
		out += fmt.format(this.getDateOut().getTime());
		return out;
		
	}
	public Integer getDays() {
		Long diff = this.getDateOut().getTimeInMillis() - this.getDateIn().getTimeInMillis();
		Long days = diff / (1000 * 3600 * 24);
		return days.intValue();
	}
}
