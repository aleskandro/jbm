/**
 * 
 */
package org.unict.ing.jbm.domain;

import java.util.ArrayList;
import java.util.List;

/**
 * @author aleskandro
 *
 */
public final class Residence {
	private List<Service>     services;
	private List<Room>        rooms;
	private List<Reservation> reservations;
	private List<RoomDescription> roomsDescriptions;
	private List<ServiceDescription> servicesDescriptions;

	// Singleton
	private static Residence instance = null;
	
	private Residence() {
		services     = new ArrayList<Service>();
		rooms        = new ArrayList<Room>();
		reservations = new ArrayList<Reservation>();
		roomsDescriptions = new ArrayList<RoomDescription>();
		servicesDescriptions = new ArrayList<ServiceDescription>();
	}
	
	public static synchronized Residence getInstance() {
		if (instance == null) {
			return instance = new Residence();
		}
		
		return instance;
	}

	Reservation registerReservation(Reservation r) {
		reservations.add(r);
		return r;
	}
	
	Reservation findReservationByIdCustomerMail(int reservationId, String CustomerMail) {
	    for (Reservation r : reservations) {
	      if(r.getId() == reservationId) {
	        if (r.getCustomer().getMailAddress().equals(CustomerMail) && 
	        		r.getState().getClass().getSimpleName().equals("Initial")) {
	        	return r;
	        }
	      }      
	    }
	    return null;
	  }
	
	Reservation findReservationByAccessKey(AccessKey key) {
		
		for(Reservation r : reservations) {
			if (r.getAccessKey() != null)
				if(r.getAccessKey().equals(key) 
						&& r.getState().getClass().getSimpleName().equals("Checkin"))
					return r;
		}
		return null;
	}
	
	public boolean addRoom(RoomDescription descr, int quantity) {
		boolean ret = true;
		while(quantity--  > 0) 
			ret = ret && rooms.add(new Room(descr));
		return ret;
	}
	
	public boolean addService(ServiceDescription s) {
		return services.add(new Service(0,s));
	}
	
	public boolean addRoomsDescription(RoomDescription descr) {
		if (!(descr.getPeopleCapacity() > 0) || descr.getPrice() == null) {
			return false;
		}
		return roomsDescriptions.add(descr);
	}

	
	public List<RoomDescription> getRoomsDescription() {
		return roomsDescriptions;
	}

	public void setRoomsDescription(List<RoomDescription> roomsDescription) {
		this.roomsDescriptions = roomsDescription;
	}

 	public boolean addServicesDescription(ServiceDescription descr) {
 		if (descr.getPrice() == null) {
			return false;
		}
		return servicesDescriptions.add(descr);
	}
	public List<ServiceDescription> getServiceDescription() {
		return servicesDescriptions;
	}

	void setServiceDescription(List<ServiceDescription> serviceDescription) {
		this.servicesDescriptions = serviceDescription;
	}
	

	public List<Service> getServices() {
		return services;
	}
	public List<Room> getRooms() {
		return rooms;
	}
	
	public List<Reservation> getReservations() {
		return reservations;
	}

	public void setServices(List<Service> services) {
		this.services = services;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}
	
}
