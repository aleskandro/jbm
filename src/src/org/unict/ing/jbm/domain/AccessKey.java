package org.unict.ing.jbm.domain;

import java.util.Random;

public class AccessKey {
	private static int lastid = 0;
	private int id;
	private String secret;
	
	public AccessKey() {
		
		this.id = ++lastid;
		this.secret = setSecretkey();
	}
	
	public AccessKey(int id, String secret) {
		this.id = id;
		this.secret = secret;
		
	}
	
	private String setSecretkey(){
        Random random = new Random();
        int key = random.nextInt(9999999);
        return (String.format("%07d%d",key,Checksum3(key)));
    }
	
    private int Checksum3(int key){
    	
        int accum=0;
        key *= 10;
        accum=accum+3*((key/10000000)%10);
        accum=accum+((key/1000000)%10);
        accum=accum+3*((key/100000)%10);
        accum=accum+((key/10000)%10);
        accum=accum+3*((key/1000)%10);
        accum=accum+((key/100)%10);
        accum=accum+((key/10)%10);   
        int digit = accum %10;
        int checksum = (10 -digit)%10;
        return checksum;
    }

	int getId() {
		return id;
	}

	String getSecret() {
		return secret;
	}
	
	@Override
	public boolean equals(Object obj) {
	    if (obj == null) 
	        return false;
	    try {
	    	AccessKey k = (AccessKey)obj;
	    	if (this.getSecret().equals(k.getSecret()) && this.getId() == k.getId())
	    		return true;
	    } catch (Exception e) {
	    	return false;
	    }
	    return false;
	}
	
	@Override
	public String toString() {
		return (this.getId() + " <=> " + this.getSecret());
	}
}
