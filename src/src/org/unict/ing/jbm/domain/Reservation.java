package org.unict.ing.jbm.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.GregorianCalendar;

public final class Reservation implements PropertyListener {
	private Integer adults;
	private Integer children;
	private DateRange dateRange;

	private List<ReservationItem> rooms; // FINAL???
	private List<ReservationItem> services;
	
	private Customer customer;
	private Invoice invoice;
	
	private AccessKey accessKey;
	private static int lastid = 0;
	
	private List<IPayment> payments;
	private List<Guest> guests;
	
	private Integer id;
	
	private ReservationState state;
	
	private List<PropertyListener> listeners;
	
	public Reservation(DateRange dateRange, Integer adults, Integer children)
	{
		setId(++lastid);
		this.adults    = adults;
		this.children  = children;
		this.setDateRange(dateRange);
		this.payments = new ArrayList<IPayment>();
		this.guests   = new ArrayList<Guest>();
		this.state    = new Initial();
		this.listeners = new ArrayList<PropertyListener>();
		this.addPropertyListener(this);
	}
	
	public void addPropertyListener(PropertyListener listener) {
		this.listeners.add(listener);
	}
	
	private void publishPropertyEvent(String name, Object value) {
		for (PropertyListener listener : listeners)
			listener.onPropertyEvent(this, name, value);
	}
	
	boolean addGuest(Guest guest) {
		if (!(guest.getNationalId().length() > 0) || guests.size() >= this.getPeopleCount())
			return false;
		return guests.add(guest);
	}
	
	AccessKey generateKey() {
		// The key must not be generated if not, at least, caution was paid
		if (this.checkForCaution() > 0)
			return null;
		
		if (this.accessKey != null) {
			return this.accessKey;
		}
		
	    AccessKey accessKey = new AccessKey();
		Checkin c = new Checkin();
		this.setState(c);
		this.setAccessKey(accessKey);
		return accessKey;
	}
	
	double checkForCaution() {
		double caution = 0;
		caution = (this.getPeopleCount() * 20) - this.getTotalPaid();
		if (caution < 0)
			caution = 0;
		return caution; 
	}
	
	Invoice generateInvoice(Invoice invoiceDetails) {
		this.setInvoice(invoiceDetails);
		this.setState(new Checkout());
		this.setAccessKey(null);
		return this.getInvoice();
	}
	
	Integer getPeopleCount() {
		return this.adults + this.children;
	}
	
	private Double getDailyTax() {
		return this.adults * this.getDateRange().getDays() * 2.0;
	}
	
	public Double getTotalAmount() {
		double sum = 0; 
		for (ReservationItem r : rooms) {
			RoomDescription d = ((Room)(r.getItem())).getDescription();
			sum += d.getTotalAmount();
		}
		
		for (ReservationItem s : services) 
			sum += s.getItem().getDescription().getTotalAmount();
		
		return sum + this.getDailyTax();
	}
	
	public double getTotalPaid() {
		double sum = 0;
		for (IPayment p : payments) {
			sum += p.getAmount();
		}
		return sum;
	}
	
	public Double getTotalToPay() {
		return this.getTotalAmount() - this.getTotalPaid();
	}
	
	void setSolution(List<Room> rooms) {
		this.rooms = new ArrayList<ReservationItem>();
		for (Room r : rooms) {
			this.rooms.add(new ReservationItem(this, r, new AgedPricingStrategy(r.getDescription().getPrice())));
		}
		this.publishPropertyEvent("reservation.solutionSetted", "");
	}
	
	void setServices(List<Service> services) {
		this.services = new ArrayList<ReservationItem>();
		for (Service s : services) {
			this.services.add(new ReservationItem(this, s, new UnaTantumPricingStrategy(s.getDescription().getPrice())));
		}
	}
	
	void setCustomer(Customer c) {
		this.customer = c;
	}
	
	public Customer getCustomer() {
		return this.customer;
	}
	
	List<ReservationItem> getSolution() {
		return this.rooms;
	}
	
	List<ReservationItem> getServices() {
		return this.services;
	}

	public Integer getId() {
		return id;
	}

	private void setId(Integer id) {
		this.id = id;
	}

	DateRange getDateRange() {
		return dateRange;
	}

	private void setDateRange(DateRange dateRange) {
		this.dateRange = dateRange;
	}
	
	public Invoice getInvoice() {
		return invoice;
	}

	void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

	AccessKey getAccessKey() {
		return accessKey;
	}

	private void setAccessKey(AccessKey accessKey) {
		this.accessKey = accessKey;
	}
	
	private boolean revokeKey() {
		
		this.accessKey = null;
		return true;
	}

	public List<ReservationItem> getAllItems() {
		List<ReservationItem> Items = new ArrayList<ReservationItem>();
		Items.addAll(this.getSolution());
		Items.addAll(this.getServices());
		return Items;
	}
	
	boolean addPayment(double amount) {
		// Cash payment
		CashPayment p = new CashPayment();
		if (p.authorize(amount)) {
			payments.add(p);
			return true;
		}
		return false;
	}

	boolean addPayment(String cNum, GregorianCalendar expiry, int cvv, String type, double amount) {
        // Credit payment 
        CreditPayment p = new CreditPayment(cNum, expiry, cvv, type);
        if (p.authorize(amount)) {
        	payments.add(p);
        	return true;
        }
        return false;
    }
	
    public String toString() {
        String out =  "Reservation n." + this.id + "\n" +
        		"Name of the customer:" + this.getCustomer() + "\n" +
                "DateRange:" + this.getDateRange() + "\n" +
        		"State: " + this.getState().toString() + "\n" +
                "PeopleCount: " + this.getPeopleCount() + "\n" +
                "Content: \n";
        for (ReservationItem r : this.getAllItems()) {
        	out += r.getItem().getDescription().toString(this);
        }
        out += "\n\n";
        out += "Total Amount: " + this.getTotalAmount() + "\n";
        out += "Total paid  : " + this.getTotalPaid()   + "\n";
        out += "Total to pay: " + this.getTotalToPay()  + "\n";
        
        out += "\nGuests:\n";
        for (Guest g : this.getGuests()) {
        	out += g.toString();
        }
        return out;
    }

	public ReservationState getState() {
		return state;
	}

	private void setState(ReservationState state) {
		this.state = state;
		this.publishPropertyEvent("reservation.state", state.getClass().getSimpleName());
	}

	@Override
	public void onPropertyEvent(Object source, String name, Object value) {
		if (source == this) {
			if (name.equals("reservation.state") && value.equals("Checkout")) {
				this.revokeKey();
			}
			
			if (name.equals("reservation.solutionSetted")) {
				this.setPeopleCount();
			}
		}
		
	}

	private void setPeopleCount() {
		int adults   = 0;
		int children = 0;
		for (ReservationItem i : this.getSolution()) {
			adults   += i.getItem().getDescription().getAdultsCapacity();
			children += i.getItem().getDescription().getChildrenCapacity();
		}
		
		this.adults = adults;
		this.children = children;
	}
}
