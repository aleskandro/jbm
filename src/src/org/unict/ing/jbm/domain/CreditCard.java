package org.unict.ing.jbm.domain;

import java.util.GregorianCalendar;

class CreditCard {
	private GregorianCalendar expiryDate;
	private String number;
	private Integer cvv;
	private String type;
	
	public CreditCard(String cNum, GregorianCalendar expiry, int cvv, String type){
		this.number = cNum;
		this.expiryDate = expiry;
		this.cvv = cvv;
		this.type = type;		
	}

	GregorianCalendar getExpiryDate() {
		return expiryDate;
	}

	String getNumber() {
		return number;
	}

	Integer getCvv() {
		return cvv;
	}

	String getType() {
		return type;
	}

	void setExpiryDate(GregorianCalendar expiryDate) {
		this.expiryDate = expiryDate;
	}

	void setNumber(String number) {
		this.number = number;
	}

	void setCvv(Integer cvv) {
		this.cvv = cvv;
	}

	void setType(String type) {
		this.type = type;
	}
	
}
