 package org.unict.ing.jbm.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class ProcessBookingHandler {
	
	private Reservation currentReservation;
	private Residence context;
	
	public ProcessBookingHandler(Residence context)
	{
		this.context = context;	
	}
	
	public boolean makeNewReservation(DateRange dateRange, Integer adults, Integer children) {
		if (dateRange.getDays() > 0 && adults > 0 && children >= 0) {
			this.currentReservation = new Reservation(dateRange, adults, children);
			return true;
		} else
			throw new RuntimeException("Invalid number of adults or children provided");
	}
	
	public boolean selectSolution(List<Room> solution) {
		this.currentReservation.setSolution(solution);
		return true;
	}
	
	public List<Service> getServices() {
		return context.getServices(); 
	}
	
	public boolean associateServices(List<Service> services) {
		this.currentReservation.setServices(services);
		return true;
	}
	
	public Reservation confirmReservation(Customer customer) {
		this.currentReservation.setCustomer(customer);
		return context.registerReservation(this.currentReservation);
	}
	
	public List<List<Room>> findSolutions() {
		List<Reservation> reservations = this.getOverlappedReservations(this.currentReservation);
		List<Room> rooms = this.getAvailableRooms(reservations);
		List<Room> startSolution = new LinkedList<Room>();
		List<List<Room>> finalSolutions = new LinkedList<List<Room>>();
		finalSolutions = this.CalculateAvailableSolutions(this.currentReservation.getPeopleCount(), rooms, startSolution, finalSolutions);
		
		SolutionsComparator comparator = new SolutionsComparator();
		finalSolutions = comparator.sortAndMerge(finalSolutions);

		return finalSolutions;
	}
	
	private List<Reservation> getOverlappedReservations(Reservation reservation) {
		List<Reservation> overlappedReservations = new ArrayList<Reservation>();
		// get the reservations in the same DateRange defined in reservation
		for (Reservation r : context.getReservations()) {
			if (r.getDateRange().overlap(reservation.getDateRange()))
				overlappedReservations.add(r);
		}
		return overlappedReservations;
	}

	private List<Room> getAvailableRooms(List<Reservation> reservations) {
		// Duplication of context.getRooms
		List<Room>        rooms         = new ArrayList<Room>(context.getRooms()); 
		List<Room>		  reservedRooms = new ArrayList<Room>();
		for (Reservation r : reservations) {
			for (ReservationItem reservedItemRoom : r.getSolution()) {
				Room room = (Room)reservedItemRoom.getItem();
				reservedRooms.add(room);
			}
		}
		rooms.removeAll(reservedRooms);
		return rooms;
	}

	
	private List<List<Room>> CalculateAvailableSolutions(Integer peopleToAccomodate, List<Room> rooms, List<Room> startSolution, List<List<Room>> finalSolutions) {
		List<Room> roomsCopy = new ArrayList<Room>(rooms);
		Iterator<Room> iterator = roomsCopy.iterator();
		while (iterator.hasNext()) {
			Room room = iterator.next();
			List<Room> tmpSolution = new ArrayList<Room>(startSolution);
			
			Integer remainder = peopleToAccomodate - room.getDescription().getPeopleCapacity();
			if (remainder < 0)
				remainder = 0;
			
			tmpSolution.add(room);
			iterator.remove();
			
			if (remainder == 0) { 
				// solution is able to accomodate all the people for this reservation
				finalSolutions.add(tmpSolution);
				//roomsCopy = new ArrayList<Room>(rooms);  // ?? 
				// Redefine roomsCopy for iteration mode: restart from startSolution to get another one
			} else { 
				// Recursion
				// Restart the function to get another room until the remainder is 0 and all the people
				// Could be accomodated in a collection of Room(s).
				this.CalculateAvailableSolutions(remainder, roomsCopy, tmpSolution, finalSolutions);
			}
		}
		return finalSolutions;
	}
	
}
