package org.unict.ing.jbm.domain;

abstract class IPayment {
	
	private double amount;
	
	abstract boolean authorize(double amount);

	double getAmount() {
		return amount;
	}

	protected void setAmount(double amount) {
		this.amount = amount;
	}
	
}
