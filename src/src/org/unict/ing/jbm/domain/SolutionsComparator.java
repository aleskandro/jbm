package org.unict.ing.jbm.domain;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class SolutionsComparator implements Comparator<List<Room>> {
	// To sort
	@Override
	public int compare(List<Room> arg0, List<Room> arg1) {
		int cap0 = 0;
		int cap1 = 0;
		
		for (Room r : arg0)
			cap0 += r.getDescription().getPeopleCapacity();
		
		for (Room r : arg1)
			cap1 += r.getDescription().getPeopleCapacity();
		
		return cap0 - cap1;
	}

	public boolean areEquals(List<Room> arg0, List<Room> arg1) {
		List<Room> largest;
		List<Room> shortest;
		
		// Get the largest solution for the outer loop so that all the rooms could be checked
		if (arg0.size() > arg1.size()) {
			largest = arg0;
			shortest = arg1;
		} else {
			largest = arg1;
			shortest = arg0;
		}
		
		for (Room r : largest) {
			boolean matched = false;
			for (Room s : shortest) {
				if (r.getDescription().equals(s.getDescription())) {
					matched = true;
					// if equals we can start to check the next room in the 'largest' solution
					break;
				}
			}
			// Not matching once is not matching at all, no needs to go ahead with next iteration
			if (!matched) 
				return false;
		}
		
		// If the code goes here, the code went through all the loops and we can assume the two args represent 
		// the same solution
		return true;
	}
	
	public List<List<Room>> sortAndMerge(List<List<Room>> solutions) {
		solutions.sort(this);
		List<List<Room>> toRemove = new ArrayList<List<Room>>();
		for (int i = 0; i < solutions.size() - 1; i++) {
			List<Room> s0 = solutions.get(i);
			for (int j = i + 1; j < solutions.size(); j++) {
				List<Room> s1 = solutions.get(j);
				// The solutions have been sorted above, so the solutions to merge are consequent
				// and we can break the loop if not equals
				if (!this.areEquals(s0, s1))  
					break;
				toRemove.add(s1);
			}
		}
		solutions.removeAll(toRemove);
		return solutions;
	}
}
