package org.unict.ing.jbm.guicli4admin;

import org.unict.ing.jbm.guicommon.CommonVC;
import org.unict.ing.jbm.guicommon.GuiHelpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.unict.ing.jbm.domain.*;

final class GuiCli extends GuiHelpers  {

	private static Residence context = Residence.getInstance();
	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	private static DateFormat edf = new SimpleDateFormat("MM/yyyy");
	
	private static void insertRoom() {
		
		String name               = readLine("Inserire un nome per la stanza: ");
		String extraInfos         = readLine("Inserire delle informazioni extra per la stanza: ");
		Integer adultsCapacity    = readInt("Inserire il numero di adulti ospitabili: ");
		Integer childrenCapacity  = readInt("Inserire il numero di bambini ospitabili: ");
		Double adultPrice         = readDouble("Inserire il prezzo per singolo adulto a notte (eur/(adulto * notte)): ");
		Double childPrice         = readDouble("Inserire il prezzo per singolo bambino a notte (eur/(bambino * notte)): ");
		Integer quantity          = readInt("Inserire il numero di stanze cosi' descritte disponibili: ");
		
		AgedPricingStrategy price = new AgedPricingStrategy(adultPrice, childPrice);
		RoomDescription d         = new RoomDescription(adultsCapacity, childrenCapacity, price, name, extraInfos);		
		
		if (!context.addRoomsDescription(d)) {
			println("Errore nella descrizione della stanza. Reinserire i dati.");
			println();
			insertRoom();
		}
		
		if (!context.addRoom(d, quantity)) {
			println("Errore  nella creazione delle stanze. Verifica i dati inseriti.");
			insertRoom();
		}
		
		println("Stanze inserite correttamente. \n");
		println(d);
	}
	
	private static void insertService() {
		String name                    = readLine("Inserire un nome per il servizio: ");
		String extraInfos              = readLine("Inserire informazioni aggiuntive per il servizio: ");
		
		UnaTantumPricingStrategy price = 
				new UnaTantumPricingStrategy(readDouble("Inserire il prezzo del servizio: "));
		ServiceDescription d           = new ServiceDescription(name, price, extraInfos);
		
		if (!context.addServicesDescription(d)) {
			println("Errore nella descrizione del servizio. Verifica i dati.");
			insertService();
		}
		
		if (!context.addService(d)) {
			println("Errore nell'aggiunta del servizio. Verifica i dati.");
			insertService();
		}
		
		println("Servizio aggiunto correttamente.");
		println(d);
	}
	
	
	private static void listReservations() {
		println(context.getReservations());
	}
	
	
	private static void mainMenu() {
		while(true) {
			Integer reply = 0;
			
			String menu = "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "+++++++++++++ jBM - Booking Manager +++++++++++++ \n" +
						  "+++++++++++++++++++++++++++++++++++++++++++++++++ \n" +
						  "|                                               | \n" +				
					      "| 1) Inserisci stanze                           | \n" +
						  "| 2) Inserisci servizio                         | \n" +
					      "| 3) Crea prenotazione                          | \n" +
					      "| 4) Lista stanze                               | \n" +
						  "| 5) Lista servizi                              | \n" +
					      "| 6) Lista prenotazioni                         | \n" +
					      "| 7) Checkin                                    | \n" +
					      "| 8) Checkout                                   | \n" +
					      "| 0) Esci                                       | \n" +
					      "+++++++++++++++++++++++++++++++++++++++++++++++++ \n\n" +
						  "--> ";
						  		
			reply = readInt(menu);		  
			switch(reply) {
			case 1:
				insertRoom();
				break;
			case 2:
				insertService();
				break;
			case 3:
				CommonVC.makeReservation();
				break;
			case 4:
				CommonVC.listRooms();
				break;
			case 5:
				CommonVC.listServices();
				break;
			case 6:
				listReservations();
				break;
			case 7:
				CommonVC.makeCheckin();
				break;
			case 8:
				CommonVC.makeCheckOut();
				break;
			case 0:
				return;
			default:
				continue;
			}
			
			pause();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		fuckedSetup();
		mainMenu();
		print("Exiting");
		sc.close();
	}

	private static void fuckedSetup() {
		CommonVC.setup(context, df, edf);
		CommonVC.seeds();
	}
}
