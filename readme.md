### ** Universit� degli studi di Catania **
#### *Dipartimento di ingegneria elettrica, elettronica ed informatica*
#### *Corso di laurea magistrale in ing. informatica*
#### **Corso di ingegneria del software (Prof. Ing. Orazio Tomarchio)**
#### **jBM**
#### Progetto finale
#### *Alessandro Di Stefano - Alessandro Sangiorgi - Salvatore Mul�* 

#### Dipendenze LaTex per GNU/Linux (Testato su Fedora 25)
 
texlive texlive-layaureo texlive-biblatex texlive-bera texlive-pdfpages texlive-appendix texlive-pdfx texlive-babel-italian texlive-glossaries texlive-tocbibind

#### Dipendenze LaTex per GNU-Linux (Testato su Devuan)

texlive texlive-latex-extra texlive-lang-italian  texlive-fonts-extra

## Compilazione relazioni LaTex

```
#!bash
cd doc/ideazione/

latex main
makeglossaries main
latex main
pdflatex main

```

## Git Tags

L'ideazione, la prima iterazione e la seconda sono associate ai tre tag omonimi.

```
## ideazione

git checkout Inception-1.0

## Iterazione 1

git checkout Iterazione1

## Iterazione 2 

git checkout Iterazione2

```

## Starting jbm

Sono disponibili due versioni della ui, in versione CLI  interattiva: 

* gui4admin, per il receptionist, che implementa tutti i casi d'uso considerati

* gui4customer, per i customer, che implementa esclusivamente i casi d'uso (tra quelli sviluppati) che possono avere come utente un customer

Le release si trovano nella directory bin/

Per avviare l'applicazione (testato su Gnu/Linux):

```
$ java -jar bin/gui4(customer|admin).jar
```